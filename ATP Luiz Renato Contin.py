#  Programa ATP Raciocínio Computacional
# Luiz Renato Contin

# Bibliotecas Importadas
from datetime import datetime

def obter_limite():
    # Entrada de Dados cargo,salário e ano_nascimento
    print ("Bem-vindo à Loja do Luiz Renato Contin")
    print ("Faremos uma análise de crédito para você")
    cargo = input("Informe o seu cargo na empresa em que trabalha atualmente: ")
    salario = float(input("Informe o seu salario: "))
    ano_nascimento = input("Informe seu ano de nascimento: ")
    print (cargo)
    print (salario)
    print (ano_nascimento)

    # Cálculo da idade aproximada
    hoje = datetime.now()
    ano_atual = hoje.strftime("%Y")
    idade_aproximada = int(ano_atual) - int(ano_nascimento)
    print("sua idade aproximada é: ",idade_aproximada)

    # Cálculo do limite de gasto
    limite_de_gasto = ((float(salario) * (float(idade_aproximada)/1000))+ 100)
    print("seu limite de gasto é:",limite_de_gasto)
    return limite_de_gasto , idade_aproximada

def verificar_produto(limite_de_gasto,idade_aproximada, soma_gastos):
    # Entrada de Dados nome do Produto e preço
    nome_do_produto = input("Informe o nome do Produto: ")
    preço_do_produto = float(input("Informe o preço do Produto: "))
    
    # verifica se cliente ainda possui limite
    soma_gastos += preço_do_produto
    
    if (soma_gastos <= limite_de_gasto):
        
        # Validação e parcelamento da compra
        relação_lim_gasto_x_preço_prod = preço_do_produto * 100 / limite_de_gasto

        if (relação_lim_gasto_x_preço_prod <= 60):
            print("Liberado!")
        elif (relação_lim_gasto_x_preço_prod > 60 and relação_lim_gasto_x_preço_prod <= 90):
            print("Liberado ao parcelar em até 2 vezes")
        elif (relação_lim_gasto_x_preço_prod > 90 and relação_lim_gasto_x_preço_prod <= 100):
            print("Liberado ao parcelar em 3 ou mais vezes")
        else:
            print("Bloqueado")

        # Calculo do desconto
        if (preço_do_produto >= 18 and preço_do_produto <= idade_aproximada):
            print ("você terá um desconto de R$ 4,00")
            preço_do_produto = preço_do_produto - 4
            print ("Preço do Produto com desconto: ",preço_do_produto)
    else:
        print("você não tem limite sobrando para comprar esse produto")
        preço_do_produto = 0 # compra não efetivada, então retorna valor zerado
    return preço_do_produto

# Chama função obter_limite, pergunta número de produtos e verifica os produtos

limite , idade_aproximada = obter_limite()
qtdade_produtos = int(input("Informe o número de produtos que deseja cadastrar: "))
contador_de_produtos = 1
soma_gastos = 0
while (contador_de_produtos <= qtdade_produtos):
    valor_gasto = float(verificar_produto(limite, idade_aproximada, soma_gastos))
    soma_gastos += valor_gasto
    contador_de_produtos += 1
